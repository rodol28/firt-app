import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Task extends Component {

    styleComplete() {
        return {
            fontSize: '20px',
            color: this.props.task.done ? 'green' : 'black',
            textDecoration: this.props.task.done ? 'line-through' : 'none'
        };
    }

    render() {

        const { task } = this.props;

        return(
            <p style={this.styleComplete()}>
                id: <b>{task.id}</b> -
                title: <b>{task.title}</b> -
                description: <b>{task.description}</b> -
                done: <b>{task.done}</b>
                <input 
                    type="checkbox" 
                    onChange={this.props.checkDone.bind(this, task.id)}
                />
                <button 
                    style={redColor} 
                    onClick={this.props.deleteTask.bind(this, task.id)}>
                X</button> 
            </p>
            );
    };
}

Task.propTypes = {
    task: PropTypes.object.isRequired
  };

const redColor = {
    fontSize: '18px',
    background: '#ff0000',
    color: '#fff',
    padding: '10px 15px',
    borderRadius: '50%',
    cursor: 'pointer'
};

export default Task;