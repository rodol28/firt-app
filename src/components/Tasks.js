import React, { Component } from 'react';
import Task from './Task';
import PropTypes from 'prop-types';

class Tasks extends Component {
    render() {
        return (
            this.props.listTasks.map(elem => 
            <Task 
                task={elem} 
                key={elem.id}
                deleteTask={this.props.deleteTask}
                checkDone={this.props.checkDone}
            />)
        );
    };
}

Tasks.propTypes = {
    listTasks: PropTypes.array.isRequired
};

export default Tasks;