import React, { Component } from 'react';


class TaskForm extends Component {

    state = {
        title: "",
        description: ""
    }

    save = e => {
        this.props.addTask(this.state.title, this.state.description);
        e.preventDefault();
    }

    handlerInputs = e => {
        // console.log(e.target.name, e.target.value);
        this.setState({
            [e.target.name]: e.target.value
        });
    };

    render() {
        
        return (
            <form onSubmit={this.save}>
                <input 
                    name="title"
                    onChange={this.handlerInputs} 
                    value={this.state.title} 
                    type="text" 
                    placeholder="White a task"/>
                <br/><br/>
                <textarea 
                    name="description"
                    onChange={this.handlerInputs} 
                    value={this.state.description} 
                    placeholder="Write a description"/>
                <br/><br/>
                <input type="submit" value="Save"/>
            </form>
        );
    };
}

export default TaskForm;